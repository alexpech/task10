import unittest
import vp2


class MyTestCase(unittest.TestCase):
    def test_something(self):
        self.assertEqual(vp2.areParanthesisBalanced("()"), True)
        self.assertEqual(vp2.areParanthesisBalanced("(){}[]"), True)
        self.assertEqual(vp2.areParanthesisBalanced("(]"), False)
        self.assertEqual(vp2.areParanthesisBalanced("([)]"), False)
        self.assertEqual(vp2.areParanthesisBalanced("{[]}"), True)


if __name__ == '__main__':
    unittest.main()
